package dev.edmt.todolist;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class AddEditPerson extends AppCompatActivity {

    DbHelper dbHelper;
    EditText name;
    EditText address;
    EditText email;
    EditText mobile;

    CharSequence textUpdate = "Person Updated";
    CharSequence textCreate = "Person Added";
    CharSequence textDelete = "Person Deleted";
    CharSequence textAlreadyExists = "Person already exists";
    CharSequence textEmptyFields = "Please fill all fields";

    boolean isEditable = false;


    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_person);

        dbHelper = new DbHelper(this);
        Intent intent = this.getIntent();
        String value = intent.getStringExtra("person");

        name = (EditText) findViewById(R.id.txtEmpName);
        address = (EditText) findViewById(R.id.txtEmpAdd);
        email = (EditText) findViewById(R.id.txtEmpCity);
        mobile = (EditText) findViewById(R.id.txtEmpMobile);

        if (!value.equals("")) {

            Person data = dbHelper.getPerson(value);

            name.setText(data.getName());
            address.setText(data.getAddress());
            email.setText(data.getEmail());
            mobile.setText(data.getMobile());

            setEditable();
        } else {

            Button button = (Button) findViewById(R.id.btnSubmit);
            button.setText("Save");

        }
    }

    private void setEditable() {
        name.setEnabled(isEditable);
        address.setEnabled(isEditable);
        email.setEnabled(isEditable);
        mobile.setEnabled(isEditable);
    }

    public void addEditPerson(View view) {
        Intent intent = this.getIntent();
        String value = intent.getStringExtra("person");
        final Intent mainActivityIntent = new Intent(this, MainActivity.class);
        if (!arePersonFieldsEmpty()) {
            if (value.equals("")) {

                Person newPerson = setPerson();

                if (!alreadyExists(dbHelper, newPerson.getName())) {
                    dbHelper.insertNewPerson(newPerson);
                    startActivity(mainActivityIntent);
                    displayMessage(textCreate);
                }
            } else {

                if (!isEditable) {
                    Button button = (Button) findViewById(R.id.btnSubmit);
                    Button button2 = (Button) findViewById(R.id.btnDelete);
                    button2.setVisibility(View.VISIBLE);
                    button.setText("Save");
                    isEditable = true;
                    setEditable();

                } else {
                    dbHelper.updatePerson(value, setPerson());
                    startActivity(mainActivityIntent);
                    displayMessage(textUpdate);
                }
            }
        }
    }

    public Person setPerson() {
        Person person = new Person();
        person.setName(String.valueOf(name.getText()));
        person.setAddress(String.valueOf(address.getText()));
        person.setEmail(String.valueOf(email.getText()));
        person.setMobile(String.valueOf(mobile.getText()));
        return person;
    }

    public boolean arePersonFieldsEmpty() {
        if (name.getText().toString().trim().length() > 0 &&
                address.getText().toString().trim().length() > 0 &&
                email.getText().toString().trim().length() > 0 &&
                mobile.getText().toString().trim().length() > 0
                ) return false;
        else {
            displayMessage(textEmptyFields);
            return true;
        }
    }

    public void deletePerson(View view) {
        Intent intent = this.getIntent();
        String person = intent.getStringExtra("person");
        final Intent mainActivityIntent = new Intent(this, MainActivity.class);
        dbHelper.deletePerson(person);
        startActivity(mainActivityIntent);
        displayMessage(textDelete);
    }

    public boolean alreadyExists(DbHelper dbHelper, String name) {
        ArrayList<String> taskList = dbHelper.getPeopleList();

        for (int i = 0; taskList.size() > i; i++) {
            if (taskList.get(i).equals(name)) {
                displayMessage(textAlreadyExists);
                return true;
            }

        }
        return false;
    }

    public void displayMessage(CharSequence message) {
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, message, duration);
        toast.show();
    }
}
