package dev.edmt.todolist;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;


public class DbHelper extends SQLiteOpenHelper {

    public static final String DB_TABLE = "Contacts";
    public static final String DB_COLUMN0 = "ID";
    public static final String DB_COLUMN1 = "TaskName";
    public static final String DB_COLUMN2 = "Address";
    public static final String DB_COLUMN3 = "Email";
    public static final String DB_COLUMN4 = "Mobile";
    private static final String DB_NAME = "108";
    private static final int DB_VER = 1;

    public DbHelper(Context context) {
        super(context, DB_NAME, null, DB_VER);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT,%s TEXT NOT NULL,%s Text,%s Text,%s Text);", DB_TABLE, DB_COLUMN0, DB_COLUMN1, DB_COLUMN2, DB_COLUMN3, DB_COLUMN4);
        db.execSQL(query);
        String init2 = String.format("CREATE TABLE %s (ID INTEGER PRIMARY KEY AUTOINCREMENT,%s TEXT NOT NULL,%s TEXT);", "admin", "User", "password");
        db.execSQL(init2);
        ContentValues values = new ContentValues();
        values.put("User", "Bolek");
        values.put("password", "123");
        db.insertWithOnConflict("admin", null, values, SQLiteDatabase.CONFLICT_REPLACE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String query = String.format("DELETE TABLE IF EXISTS %s", DB_TABLE);
        db.execSQL(query);
        onCreate(db);

    }

    public User getAdmin() {
        SQLiteDatabase db = this.getWritableDatabase();
        User user = new User();
        Cursor  cursor = db.query("admin", new String[]{"User", "password"}, null,
                null, null, null, null, null);
        cursor.moveToPosition(0);
        user.setName(cursor.getString(0));
        user.setPassword(cursor.getString(1));
        cursor.close();
        db.close();
        return user;
    }


    public void insertNewPerson(Person person) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        Cursor cursor = db.query(DB_TABLE, new String[]{DB_COLUMN1}, null, null, null, null, null);
        while (cursor.moveToNext()) {

            cursor.getString(0);
            Log.v("existing name", cursor.getString(0));
        }

        values.put(DB_COLUMN1, person.getName());
        values.put(DB_COLUMN2, person.getAddress());
        values.put(DB_COLUMN3, person.getEmail());
        values.put(DB_COLUMN4, person.getMobile());
        db.insertWithOnConflict(DB_TABLE, null, values, SQLiteDatabase.CONFLICT_REPLACE);
        db.close();
    }

    public void updatePerson(String task, Person person) {

        String idToUpdate = getPerson(task).getId();
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(DB_COLUMN1, person.getName());
        values.put(DB_COLUMN2, person.getAddress());
        values.put(DB_COLUMN3, person.getEmail());
        values.put(DB_COLUMN4, person.getMobile());
        db.update(DB_TABLE, values, "ID = ?", new String[]{idToUpdate});
        db.close();
    }

    public void deletePerson(String person) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(DB_TABLE, DB_COLUMN1 + " = ?", new String[]{person});
        db.close();

    }

    public Person getPerson(String personName) {
        SQLiteDatabase db = this.getWritableDatabase();

        Person person = new Person();
        Cursor cursor = db.query(DB_TABLE, new String[]{DB_COLUMN0, DB_COLUMN1, DB_COLUMN2, DB_COLUMN3, DB_COLUMN4}, DB_COLUMN1 + " LIKE ?",
                new String[]{personName}, null, null, null, null);

        cursor.moveToPosition(0);
        person.setId(cursor.getString(0));
        person.setName(cursor.getString(1));
        person.setAddress(cursor.getString(2));
        person.setEmail(cursor.getString(3));
        person.setMobile(cursor.getString(4));

        cursor.close();
        db.close();
        return person;
    }

    public ArrayList<String> getPeopleList() {

        ArrayList<String> taskList = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(DB_TABLE, new String[]{DB_COLUMN1}, null, null, null, null, null);
        while (cursor.moveToNext()) {
            int index = cursor.getColumnIndex(DB_COLUMN1);
            taskList.add(cursor.getString(index));
        }
        cursor.close();
        db.close();
        return taskList;
    }
}
